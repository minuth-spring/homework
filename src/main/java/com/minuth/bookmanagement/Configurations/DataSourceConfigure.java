package com.minuth.bookmanagement.Configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfigure
{
    @Bean
    @Profile("my-pgsql")
    public DataSource dataSource()
    {
        DriverManagerDataSource source=new DriverManagerDataSource();
        source.setDriverClassName("org.postgresql.Driver");
        source.setUrl("jdbc:postgresql://localhost:5432/BookManagementDB");
        source.setUsername("postgres");
        source.setPassword("minuth321");
        return source;
    }
    @Bean
    @Profile("my-h2")
    public DataSource inMemoryDB() {
        EmbeddedDatabaseBuilder databaseBuilder = new EmbeddedDatabaseBuilder();
        databaseBuilder.setType(EmbeddedDatabaseType.H2);

        databaseBuilder.addScript("static/db/schema.sql");

        databaseBuilder.addScript("static/db/data.sql");

        return databaseBuilder.build();
    }

}
