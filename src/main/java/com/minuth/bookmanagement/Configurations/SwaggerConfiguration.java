package com.minuth.bookmanagement.Configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration
{
    @Bean
    public Docket docket()
    {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
//                .apis(RequestHandlerSelectors.any()) //scan all packages
                .apis(RequestHandlerSelectors.basePackage("com.minuth.bookmanagement.restcontroller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }
    private ApiInfo apiInfo()
    {
        List<VendorExtension>vendorExtensions=new ArrayList<>();
        Contact c=new Contact("Minuth","www.minuth.ms.com","minuthprom321@gmail.com");
        ApiInfo apiinfo=new ApiInfo(
                "Book Management System",
                "provide book manangement systemm",
                "Version 0.1",
                "Term of service",
                c,
                "Copy rigth",
                "google.com",
                vendorExtensions
        );
        return apiinfo;
    }
}
