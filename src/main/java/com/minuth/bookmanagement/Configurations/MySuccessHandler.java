package com.minuth.bookmanagement.Configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Configuration
public class MySuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        System.out.println(authentication.getAuthorities());
        String redirectURI= "";//(String) httpServletRequest.getSession().getAttribute("URI");
        for (GrantedAuthority grantedAuthority:
                authentication.getAuthorities()) {
            System.out.println();
            if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
                redirectURI = "/admin";
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
                redirectURI = "/user";
                break;
            }
        }
        System.out.println(redirectURI);
        httpServletResponse.sendRedirect(redirectURI);
    }
}
