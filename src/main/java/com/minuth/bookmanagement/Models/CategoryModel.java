package com.minuth.bookmanagement.Models;


import java.util.List;

public class CategoryModel
{
    private Integer id;
    private String name;
    private List<BookModel>bookModelList;
    public List<BookModel> getBookModelList() {
        return bookModelList;
    }

    public void setBookModelList(List<BookModel> bookModelList) {
        this.bookModelList = bookModelList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
