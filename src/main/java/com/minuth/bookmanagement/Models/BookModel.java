package com.minuth.bookmanagement.Models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class BookModel
{
    private Integer id;
//    @NotNull()
    @NotEmpty(message = "is require!!!")
    private String title;
    private String author;
    private String publisher;
    private CategoryModel categoryModel;
    private String image_url;
    public BookModel()
    {
        categoryModel=new CategoryModel();
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", category=" + categoryModel.getName() +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}
