package com.minuth.bookmanagement.Controllers;
import com.minuth.bookmanagement.Models.CategoryModel;
import com.minuth.bookmanagement.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CategoryController
{
    CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/catelist")
    public String index(Model model) {
        model.addAttribute("categories", categoryService.getAll());
        return "category/cate_list";
    }

    @GetMapping("/catecreate")
    public String create(Model model) {
        model.addAttribute("maxId", categoryService.getMaxId());
        model.addAttribute("category", new CategoryModel());
        return "category/cate_create";
    }

    @PostMapping("/catecreate/submit")
    public String submitCreate(@ModelAttribute CategoryModel categoryModel){
        categoryService.insert(categoryModel);
        return "redirect:/catelist";
    }

    @GetMapping("/cateupdate{id}")
    public String update(@RequestParam("id") Integer id, Model model)
    {
        model.addAttribute("categorie", categoryService.getOne(id));
        return "category/cate_update";
    }
    @PostMapping("/cateupdate/submit")
    public String submitUpdate(@ModelAttribute CategoryModel categoryModel){
        categoryService.update(categoryModel);
        return "redirect:/catelist";
    }
    @GetMapping("/catedelete{id}")
    public String delete(@RequestParam("id") Integer id)
    {
        System.out.println(id);
        categoryService.delete(id);
        return "redirect:/catelist";
    }

}
