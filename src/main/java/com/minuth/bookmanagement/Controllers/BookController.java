package com.minuth.bookmanagement.Controllers;

import com.minuth.bookmanagement.Models.BookModel;
import com.minuth.bookmanagement.Services.BookService;
import com.minuth.bookmanagement.Services.CategoryService;
import com.minuth.bookmanagement.Services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
@Controller
public class BookController {
    BookService bookService;
    CategoryService categoryService;
    UploadService uploadService;

    @Autowired
    public BookController(BookService bookService, CategoryService categoryService,UploadService uploadService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
        this.uploadService=uploadService;
    }
    @GetMapping("/index")
    public String index(Model model)
    {
        model.addAttribute("totalBook",bookService.getAll().size());
        model.addAttribute("totalCategory",categoryService.getAll().size());
        return "index";
    }
    @GetMapping("/booklist")
    public String bookList(Model model) {
        model.addAttribute("books", bookService.getAll());
        return "book/book_list";
    }

    @GetMapping("/bookcreate")
    public String create(Model model) {
        model.addAttribute("bookModel", new BookModel());
        model.addAttribute("maxId", bookService.getMaxId());
        model.addAttribute("categories", categoryService.getAll());
        return "book/book_create";
    }

    @PostMapping("/bookcreate/submit")
    public String submitCreate(@Valid BookModel bookModel, BindingResult result, @RequestParam("file") MultipartFile file,Model model) throws IOException {
        if(result.hasErrors())
        {
            model.addAttribute("maxId", bookService.getMaxId());
            model.addAttribute("categories", categoryService.getAll());
            return "book/book_create";
        }
        else
        {
            String filename=uploadService.upload(file,"bookImages/");
            bookModel.setImage_url(uploadService.getFolder()+filename);
            bookService.add(bookModel);
            return "redirect:/booklist";
        }
    }

    @GetMapping("/bookupdate{id}")
    public String update(@RequestParam("id") Integer id, Model model)
    {
        model.addAttribute("book",bookService.getOne(id));
        model.addAttribute("categories", categoryService.getAll());
        return "book/book_update";
    }
    @PostMapping("/bookupdate/submit")
    public String submitUpdate(@ModelAttribute BookModel bookModel,@RequestParam("file") MultipartFile file) throws IOException {
        if(!file.isEmpty())
        {
            String filename=uploadService.upload(file,"bookImages/");
            bookModel.setImage_url(uploadService.getFolder()+filename);
        }
        bookService.update(bookModel);
        return "redirect:/booklist";
    }
    @GetMapping("/bookdelete{id}")
    public String delete(@RequestParam("id") Integer id)
    {
        bookService.delete(id);
        return "redirect:/booklist";
    }
    @GetMapping("/bookview{id}")
    public String view(@RequestParam("id") Integer id,Model model)
    {
        model.addAttribute("book",bookService.getOne(id));
        return "book/book_view";
    }

}
