package com.minuth.bookmanagement.restcontroller;

import com.minuth.bookmanagement.Models.BookModel;
import com.minuth.bookmanagement.Services.BookService;
import com.minuth.bookmanagement.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BookRestController
{
    private BookService bookService;
    private CategoryService categoryService;

    @Autowired
    public BookRestController(BookService bookService, CategoryService categoryService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
    }
    @GetMapping("/api/book/index")
    public Map<String,List<BookModel>> getAll()
    {
        Map<String,List<BookModel>>books=new HashMap<>();
        books.put("books",bookService.getAll());
        return books;
    }
    @GetMapping("/api/book/delete{id}")
    public Map<String,Map<String,Object>> delete(@RequestParam Integer id)
    {
        Map<String,Map<String,Object>> response=new HashMap<>();
        Map<String,Object>status=new HashMap<>();
        boolean isdeleted=bookService.delete(id);
        if(isdeleted)
        {
            status.put("status","success");
            status.put("deleted",true);
        }
        else
        {
            status.put("status","failed");
            status.put("deleted",false);
        }
        response.put("book",status);
        return response;
    }

    @PostMapping("/api/book/add")
    public  Map<String ,Map<String,Object>> insert(@RequestBody BookModel model)
    {
        boolean isSuccess=bookService.add(model);
        Map<String,Map<String,Object>>response=new HashMap<>();
        Map<String,Object>status=new HashMap<>();
        if(isSuccess)
        {
            status.put("status","success");
            status.put("added",true);
        }
        else
        {
            status.put("status","failed");
            status.put("added",false);
        }
        response.put("book",status);

        return response;
    }

    @GetMapping("/admin")
    public String admin()
    {
        return "Welcome Admin Page!!!";
    }

    @GetMapping("/user")
    public String user()
    {
        return "Welcome User Page!!!";
    }


}
