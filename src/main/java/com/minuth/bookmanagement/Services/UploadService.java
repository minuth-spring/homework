package com.minuth.bookmanagement.Services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UploadService
{
    String singleUpload(MultipartFile file,String folder) throws IOException;
    String upload(MultipartFile file) throws IOException;
    String upload(MultipartFile file,String folder) throws IOException;
    String getFolder();
}
