package com.minuth.bookmanagement.Services;

import com.minuth.bookmanagement.Models.BookModel;

import java.util.List;

public interface BookService
{
    List<BookModel>getAll();
    BookModel getOne(Integer id);
    boolean add(BookModel model);
    boolean update(BookModel model);
    boolean delete(Integer id);
    Integer getMaxId();

}
