package com.minuth.bookmanagement.Services.Imps;

import com.minuth.bookmanagement.Models.CategoryModel;
import com.minuth.bookmanagement.Repositories.CategoryRepository;
import com.minuth.bookmanagement.Services.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {

    private CategoryRepository repository;

    public CategoryServiceImp(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<CategoryModel> getAll() {
        return repository.getAll();
    }

    @Override
    public CategoryModel getOne(Integer id) {
        return repository.getOne(id);
    }

    @Override
    public void insert(CategoryModel model) {
        repository.add(model);
    }

    @Override
    public void update(CategoryModel model) {
        repository.update(model);
    }

    @Override
    public void delete(Integer id) {
        repository.delete(id);
    }

    @Override
    public Integer getMaxId() {
        return repository.getMaxId()==null?1:repository.getMaxId()+1;
    }
}
