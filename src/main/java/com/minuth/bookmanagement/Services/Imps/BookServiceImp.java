package com.minuth.bookmanagement.Services.Imps;

import com.minuth.bookmanagement.Models.BookModel;
import com.minuth.bookmanagement.Repositories.BookRepository;
import com.minuth.bookmanagement.Services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImp implements BookService {
    private BookRepository repository;

    @Autowired
    public BookServiceImp(BookRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<BookModel> getAll() {
        return repository.getAll();
    }

    @Override
    public BookModel getOne(Integer id) {
        return repository.getOne(id);
    }

    @Override
    public boolean add(BookModel model) {
        return repository.add(model);
    }

    @Override
    public boolean update(BookModel model) {
        return repository.update(model);
    }

    @Override
    public boolean delete(Integer id) {
        return repository.delete(id);
    }

    @Override
    public Integer getMaxId() {
        if(repository.getMaxId()==null)
        {
            return 1;
        }
        return repository.getMaxId()+1;
    }
}
