package com.minuth.bookmanagement.Services.Imps;

import com.minuth.bookmanagement.Services.UploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class UploadServiceImp implements UploadService {

    private String path="C:/bookmanagement/";
    private File file;
    private String folder;
    @Override
    public String singleUpload(MultipartFile file, String folder) throws IOException {
        byte[]bytes=file.getBytes();
        String fileName=file.getOriginalFilename();
        String extention=fileName.substring(fileName.lastIndexOf('.'));
        fileName=UUID.randomUUID()+extention;
        this.folder=folder;
        path+=folder;
        this.file=new File(path);
        if(!this.file.exists())
        {
            this.file.mkdirs();
        }
        Files.write(Paths.get(path,fileName),bytes);
        path="C:/bookmanagement/";
        return fileName;
    }

    @Override
    public String upload(MultipartFile file) throws IOException {
        return singleUpload(file,"");
    }

    @Override
    public String upload(MultipartFile file, String folder) throws IOException {
        return singleUpload(file,folder);
    }

    @Override
    public String getFolder() {
        return "/images-pp/"+folder;
    }

}
