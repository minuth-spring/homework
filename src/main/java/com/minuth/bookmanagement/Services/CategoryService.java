package com.minuth.bookmanagement.Services;

import com.minuth.bookmanagement.Models.CategoryModel;

import java.util.List;

public interface CategoryService
{
    List<CategoryModel>getAll();
    CategoryModel getOne(Integer id);
    void insert(CategoryModel model);
    void update(CategoryModel model);
    void delete(Integer id);
    Integer getMaxId();
}
