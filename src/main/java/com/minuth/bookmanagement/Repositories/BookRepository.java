package com.minuth.bookmanagement.Repositories;

import com.minuth.bookmanagement.Models.BookModel;
import com.minuth.bookmanagement.Models.CategoryModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface BookRepository {

    @Select("select * from books b inner join categories c on b.cate_id=c.id")
    @Results({
            @Result(column = "id",property ="id"),
            @Result(column = "title",property ="title"),
            @Result(column = "author",property ="author"),
            @Result(column = "publisher",property ="publisher"),
            @Result(column = "cate_id",property ="categoryModel",one=@One(select = "getCategory")),
            @Result(column = "image_url",property ="image_url")

    })
    List<BookModel>getAll();

    @Select("select * from books b inner join categories c on b.cate_id=c.id where b.id=#{id}")
    @Results({
            @Result(column = "id",property ="id"),
            @Result(column = "title",property ="title"),
            @Result(column = "author",property ="author"),
            @Result(column = "publisher",property ="publisher"),
            @Result(column = "cate_id",property ="categoryModel",one=@One(select = "getCategory")),
            @Result(column = "image_url",property ="image_url")

    })

    BookModel getOne(Integer id);

    @Select("Select * from categories where id=#{id}")
    CategoryModel getCategory(Integer id);

    @Insert("insert into books(title,author,publisher,cate_id,image_url) values(#{title},#{author},#{publisher},#{categoryModel.id},#{image_url})")
    boolean add(BookModel model);

    @Update("Update books set title=#{title},author=#{author},publisher=#{publisher},cate_id=#{categoryModel.id},image_url=#{image_url} where id=#{id}")
    boolean update(BookModel model);

    @Delete("delete from books where id=#{id}")
    boolean delete(Integer id);

    @Select("select max(id) from books")
    Integer getMaxId();

}
