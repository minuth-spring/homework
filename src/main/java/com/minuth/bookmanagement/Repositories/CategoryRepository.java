package com.minuth.bookmanagement.Repositories;
import com.minuth.bookmanagement.Models.CategoryModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CategoryRepository
{
    @Select("select * from categories")
    List<CategoryModel>getAll();
    @Select("select * from categories where id=#{id}")
    CategoryModel getOne(Integer id);
    @Insert("insert into categories(name) values(#{name})")
    void add(CategoryModel model);
    @Update("Update table categories set name=#{name} where id=#{id}")
    void update(CategoryModel model);
    @Delete("delete from categories where id=#{id}")
    void delete(Integer id);
    @Select("select max(id) from categories")
    Integer getMaxId();
}
